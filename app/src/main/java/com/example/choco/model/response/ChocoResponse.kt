package com.example.choco.model.response


import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.choco.utils.toTime
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize



data class ChocoResponse(
    @SerializedName("data")
    var data: ArrayList<Data> = arrayListOf()) {
}

@Parcelize
@Entity(tableName = "movie_database")
data class Data(
    @PrimaryKey(autoGenerate = true)
    @SerializedName("drama_id")
    var dramaId: Int = 0,
    @SerializedName("created_at")
    @ColumnInfo(name = "createdAt")
    var createdAt: String = "",
    @SerializedName("name")
    @ColumnInfo(name = "name")
    var name: String = "",
    @SerializedName("rating")
    @ColumnInfo(name = "rating")
    var rating: Double = 0.0,
    @SerializedName("thumb")
    @ColumnInfo(name = "thumb")
    var thumb: String = "",
    @SerializedName("total_views")
    @ColumnInfo(name = "totalViews")
    var totalViews: Int = 0
):Parcelable {

}