package com.example.choco.model.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.choco.model.response.Data
import io.reactivex.Completable

@Dao
interface MovieDao {

    /**
     * Get a user by id.
     * @return the user from the table with a specific id.
     */
    @Query("SELECT * FROM movie_database WHERE dramaId = :dramaId")
    fun getMovieData(dramaId: Int): Data

    @Query("SELECT * FROM movie_database")
    fun getAllMovieData(): List<Data>

    /**
     * Insert a user in the database. If the user already exists, replace it.
     * @param movie the user to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovieData(movie: ArrayList<Data>): Completable

    /**
     * Delete all users.
     */
    @Query("DELETE FROM movie_database")
    fun deleteAllMovies()
}