package com.example.choco.apiService

import com.example.choco.model.response.ChocoResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
interface ApiService {
        @GET("interview/dramas-sample.json")
        fun fetchData(): Single<Response<ChocoResponse>>
}