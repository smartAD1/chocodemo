package com.example.choco.apiService

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitManager {
    private val okhttpLog = HttpLoggingInterceptor()
        .setLevel(HttpLoggingInterceptor.Level.BODY)
    private val okHttpBuilder = OkHttpClient.Builder().
            addInterceptor(okhttpLog).build()
    fun getRetrofit(): ApiService {
        return Retrofit.Builder()
            .baseUrl("https://static.linetv.tw/")
            .client(okHttpBuilder)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(ApiService::class.java)
    }
}