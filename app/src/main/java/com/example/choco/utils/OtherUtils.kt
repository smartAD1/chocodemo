package com.example.choco.utils

import android.content.Context
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide

fun String.loadImg(context: Context,view: ImageView) = Glide.with(context).load(this).into(view)