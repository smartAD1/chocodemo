package com.example.choco.utils

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

fun String.toTime(): String {
    val utcFormat: DateFormat =
        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
    val date: Date = utcFormat.parse(this) as Date
    val pstFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    return pstFormat.format(date)
}