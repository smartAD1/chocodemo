package com.example.choco.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.choco.model.local.MovieDao
import com.example.choco.repository.DataRepository
import com.example.choco.viewModel.MovieDataViewModel

class ViewModelFactory(private val movieDao: MovieDao): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        with(modelClass) {
             when {
                isAssignableFrom(MovieDataViewModel::class.java) ->
                    MovieDataViewModel(dataRepository = DataRepository(),movieDao = movieDao)

                 else -> throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
             }
        }as T
}