package com.example.choco.repository


import com.example.choco.model.response.ChocoResponse
import io.reactivex.Single
import retrofit2.Response

interface RepositoryInterFace {
    
    fun fetchRemoteData(): Single<Response<ChocoResponse>>
}