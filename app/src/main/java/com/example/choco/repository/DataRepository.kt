package com.example.choco.repository

import com.example.choco.apiService.RetrofitManager
import com.example.choco.model.response.ChocoResponse
import io.reactivex.Single
import retrofit2.Response

class DataRepository: RepositoryInterFace {
    private val retrofit = RetrofitManager.getRetrofit()

    override fun fetchRemoteData(): Single<Response<ChocoResponse>> =
        retrofit.fetchData()

}