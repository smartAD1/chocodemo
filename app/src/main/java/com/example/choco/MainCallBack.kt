package com.example.choco

import com.example.choco.model.response.Data

interface MainCallBack {

    fun gotoNextPage(data: Data)
}