package com.example.choco.statusClass

import com.example.choco.model.response.Data

sealed class ChocoStatus {
    data class Success(val data: ArrayList<Data>) : ChocoStatus()
    data class Failure(val message: String) : ChocoStatus()
    data class TimeOut(val message: String) : ChocoStatus()
    data class NoNewWork(val data: ArrayList<Data>) : ChocoStatus()
}
