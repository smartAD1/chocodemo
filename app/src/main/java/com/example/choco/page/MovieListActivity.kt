package com.example.choco.page

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.choco.MainCallBack
import com.example.choco.R
import com.example.choco.adapter.MovieListAdapter
import com.example.choco.model.local.MovieDataBase
import com.example.choco.model.response.Data
import com.example.choco.statusClass.ChocoStatus
import com.example.choco.utils.ViewModelFactory
import com.example.choco.viewModel.MovieDataViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MovieListActivity : AppCompatActivity(), MainCallBack {

    companion object {
        var tempList = arrayListOf<Data>()
    }

    private lateinit var movieDataViewModel: MovieDataViewModel
    private val movieListAdapter: MovieListAdapter by lazy {
        MovieListAdapter(this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        weatherLists.apply {
            layoutManager = LinearLayoutManager(this@MovieListActivity)
            adapter = movieListAdapter
        }
        val movieDao = MovieDataBase.getInstance(this).movieDao()
        movieDataViewModel =
            ViewModelProvider(this, ViewModelFactory(movieDao)).get(MovieDataViewModel::class.java)
        movieDataViewModel.fetchRemoteData()
        initViewModel()
        btnEvent()
    }

    private fun btnEvent() {
        btn_fetch_remote_data.setOnClickListener {
            movieDataViewModel.fetchRemoteData()
        }
        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false;
            }

            override fun onQueryTextChange(newText: String): Boolean {
               movieListAdapter.filter.filter(newText)
                return false;
            }
        })
    }

    private fun initViewModel() {
        movieDataViewModel.run {
            chocoLiveData.observe(this@MovieListActivity, {
                when (it) {
                    is ChocoStatus.Success -> addInData(it.data)
                    is ChocoStatus.NoNewWork -> {
                        "目前離線".showToast()
                        addInData(it.data)
                    }

                    is ChocoStatus.Failure -> {
                        it.message.showToast()
                        btn_fetch_remote_data.visibility = View.VISIBLE
                    }

                    is ChocoStatus.TimeOut -> {
                        it.message.showToast()
                        btn_fetch_remote_data.visibility = View.VISIBLE
                    }
                }
            })
        }
    }

    private fun addInData(it: ArrayList<Data>) {
        movieListAdapter.data = it
        tempList.addAll(it)
        movieListAdapter.notifyDataSetChanged()
        weatherLists.visibility = View.VISIBLE
        btn_fetch_remote_data.visibility = View.GONE
    }

    override fun gotoNextPage(data: Data) {
        val intent = Intent(this, MovieContentActivity::class.java).apply {
            val bundle = bundleOf()
            bundle.putParcelable("movie", data)
            putExtras(bundle)
        }
        startActivity(intent)
    }

    private fun String.showToast() {
        Toast.makeText(this@MovieListActivity, this, Toast.LENGTH_SHORT).show()
    }
}