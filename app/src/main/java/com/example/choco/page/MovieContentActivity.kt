package com.example.choco.page

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.example.choco.R
import com.example.choco.model.response.Data
import com.example.choco.utils.loadImg
import kotlinx.android.synthetic.main.activity_page2.*

class MovieContentActivity : AppCompatActivity() {
    private var data: Data? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page2)
        intent?.let {
         data = it.getParcelableExtra("movie") as? Data
        }
        data.takeIf { it!=null }?.run {
            loadData(this)
        }
    }

    private fun loadData(data: Data) {
        data.thumb.loadImg(this,ig_content_movie)
        tv_content_movie_name.text = getString(R.string.movie_name,data.name)
        tv_content_movie_created.text = getString(R.string.movie_created,data.createdAt)
        tv_content_movie_rating.text = getString(R.string.movie_rating, data.rating.toString())
        tv_content_movie_total_views.text = getString(R.string.movie_totalViews, data.totalViews.toString())
        if (data.rating > 0.0) iv_movie_start.visibility = View.VISIBLE
    }



}
