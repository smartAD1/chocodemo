package com.example.choco.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.choco.model.local.MovieDao
import com.example.choco.model.response.Data
import com.example.choco.repository.RepositoryInterFace
import com.example.choco.statusClass.ChocoStatus
import com.example.choco.utils.addcomp
import io.reactivex.Single
import io.reactivex.SingleOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class MovieDataViewModel(
    private val dataRepository: RepositoryInterFace,
    private val movieDao: MovieDao
) : BaseViewModel() {
    val chocoLiveData: LiveData<ChocoStatus> get() = _chocoStatus
    private val _chocoStatus = MutableLiveData<ChocoStatus>()
    fun fetchRemoteData() {
        dataRepository.fetchRemoteData().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.isSuccessful) {
                    it.body()?.let {
                        addTORoomData(it.data)
                        _chocoStatus.postValue(ChocoStatus.Success(it.data))
                    }
                } else {
                    getAllMovieData()
                }
            }, {
                getAllMovieData()
            }).addcomp(compositeDisposable)
    }

    fun getAllMovieData() {
        Single.create(SingleOnSubscribe<List<Data>> {
            val list = movieDao.getAllMovieData()
            it.onSuccess(list)
        }).subscribeOn(Schedulers.io()).subscribe({
            val list = arrayListOf<Data>()
            list.addAll(it)
            if (it.isNotEmpty()) _chocoStatus.postValue(ChocoStatus.NoNewWork(list))
            else _chocoStatus.postValue(ChocoStatus.Failure("沒有資料"))
        }, {
            _chocoStatus.postValue(ChocoStatus.TimeOut("${it.message}"))
        }).addcomp(compositeDisposable)
    }

    private fun addTORoomData(data: ArrayList<Data>) =
        movieDao.insertMovieData(data).subscribeOn(Schedulers.io()).subscribe({}, {})
}