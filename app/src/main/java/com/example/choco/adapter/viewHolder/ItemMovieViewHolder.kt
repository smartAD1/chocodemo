package com.example.choco.adapter.viewHolder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.choco.MainCallBack
import com.example.choco.R
import com.example.choco.model.response.Data
import com.example.choco.utils.toTime
import kotlinx.android.synthetic.main.item_movie.view.*

class ItemMovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun setData(data: ArrayList<Data>, position: Int,
        callBack: MainCallBack) = itemView.apply {
        data[position].let { mData ->
            val time = mData.createdAt.toTime()
            Glide.with(context).load(mData.thumb).centerInside().into(iv_movie_img)
            tv_movie_name.text = context.getString(R.string.movie_name, mData.name)
            tv_movie_created.text = context.getString(R.string.movie_created,time)
            tv_movie_rating.text = context.getString(R.string.movie_rating,
                mData.rating.toString())
            setOnClickListener {
                data[position].createdAt = time
                callBack.gotoNextPage(data[position])
            }
        }
    }
}