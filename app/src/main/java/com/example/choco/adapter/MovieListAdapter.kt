package com.example.choco.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.example.choco.MainCallBack
import com.example.choco.R
import com.example.choco.adapter.viewHolder.ItemMovieViewHolder
import com.example.choco.model.response.Data
import com.example.choco.page.MovieListActivity.Companion.tempList

class MovieListAdapter(private val callBack: MainCallBack) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {
    var data = arrayListOf<Data>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        ItemMovieViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ItemMovieViewHolder -> holder.setData(data, position, callBack)
        }
    }

    override fun getFilter(): Filter = filter

    private var filter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val list = arrayListOf<Data>()
            if (constraint == null || constraint.isEmpty()) {
                data.addAll(tempList)
                notifyDataSetChanged()
            } else {
                val keyWord = "$constraint".toLowerCase()
                data.forEach {
                    if (it.name.toLowerCase().contains(keyWord)) {
                        list.add(it)
                    }
                }
            }
            val result = FilterResults()
            result.values = list
            return result
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            data.clear()
            val mData = results?.values as? ArrayList<Data>
            if (results?.values == null || mData?.isEmpty()!!) {
                data.addAll(tempList)
            } else {
                data.clear()
                mData.forEach{
                    data.add(it)
                }
                notifyDataSetChanged()
            }
        }
    }
}